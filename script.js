const burger = document.querySelector('.nav__burger');
const cross = document.querySelector('.nav__cross');
const menu = document.querySelector('.nav__ul');

function toggleOpen() {
    menu.classList.toggle('open');
    cross.classList.toggle('open');
    burger.classList.toggle('open');
}

burger.onclick = () => {
    toggleOpen();

};

cross.onclick = () => {
    toggleOpen();

};



